<?php

namespace App\Controller;

use App\Form\SearchType;
use App\Service\PageCrawler\PageCrawlerFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function index(Request $request)
    {
        $form = $this->createForm(SearchType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $pageCrawler = PageCrawlerFactory::create($form->get('url')->getData());
        }

        return $this->render(
            'index/index.html.twig',
            [
                'form' => $form->createView(),
                'pageCrawler' => isset($pageCrawler) ? $pageCrawler : false
            ]
        );
    }
}
