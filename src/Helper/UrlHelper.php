<?php

namespace App\Helper;


class UrlHelper
{
    /**
     * Check for link like mailto: tel:
     *
     * @param string $url
     *
     * @return bool
     */
    public static function isContactUrl(string $url)
    {
        return preg_match("/^(tel|mailto):/", $url);
    }


}