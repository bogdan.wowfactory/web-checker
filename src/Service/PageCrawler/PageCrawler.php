<?php

namespace App\Service\PageCrawler;


use GuzzleHttp\Client;
use Spatie\Browsershot\Browsershot;
use Symfony\Component\DomCrawler\Crawler;

class PageCrawler
{
    /** @var  Crawler $crawler */
    protected $crawler;

    /** @var  string $url */
    protected $url;

    /** @var array $googleAnaliticsDetector */
    protected $googleAnaliticsDetector = [ 'www.google-analytics.com/analytics.js'];

    /** @var  string $title */
    public $title;

    /** @var AnchorCrawler $anchor */
    public $anchor;

    /** @var  bool $secured */
    public $secured;

    /** @var  bool $googleAnalytics */
    public $googleAnalytics = false;

    /**
     * PageCrawler constructor.
     *
     * @param string $url
     */
    public function __construct(string $url)
    {
        $this->url = $url;

        $this->init($url);
    }

    /**
     * @param string $url
     *
     * @throws \Exception
     */
    public function init(string $url)
    {
        try {
            $html = Browsershot::url($url)->noSandbox()->bodyHtml();
            $this->readHtml($html, $url);
        } catch (\Exception $e) {
            //this will throw an error if if you try to access a https website that doesn't have https
            //you can check this with https://www.wowfactory.ro
            throw new \Exception('Invalid URL');
        }
    }

    /**
     * @param string $html
     */
    public function readHtml(string $html, string $url)
    {
        $this->crawler = new Crawler($html, $url);

        $this->setTitle();
        $this->setAnchor();
        $this->setSecured();
        $this->setGoogleAnalytics($html);
    }

    protected function setTitle()
    {
        $title = $this->crawler->filter('head > title')->first();

        if ($title) {
            $this->title = $title->text();
        }
    }

    protected function setAnchor()
    {
        $this->anchor = new AnchorCrawler($this->crawler, $this->url);
    }

    protected function setSecured()
    {
        //also see init method exception
        //TODO We can also check all urls, images src, scripts src to see if all are with https similar with googleAnalytics check (see setGoogleAnalytics method)
        $this->secured = parse_url($this->url, PHP_URL_SCHEME) == 'https' ? true : false;
    }

    /**
     * @param string $html
     */
    protected function setGoogleAnalytics(string $html)
    {
        $this->checkForGoogleAnalyticsInString($html);

        if(!$this->googleAnalytics){
            $this->checkForGoogleAnalyticsInScripts();
        }
    }

    /**
     * @param string $html
     */
    protected function checkForGoogleAnalyticsInString(string $html)
    {
        //check if is inside of an script tag
        foreach($this->googleAnaliticsDetector as $gaItem){
            if (strpos($html, $gaItem) !== false ){
                $this->googleAnalytics = true;
                return;
            }
        }
    }

    protected function checkForGoogleAnalyticsInScripts()
    {
        $client = new Client();
        $this->crawler->filter('script')->reduce(function (Crawler $node, $i) use ($client) {
            try{
                if (!$url = $node->attr('src')) {
                    return;
                }
                $response = $client->get($node->attr('src'));
                $content = $response->getBody()->getContents();
                if(!$this->googleAnalytics) {
                    $this->checkForGoogleAnalyticsInString($content);
                }
            }catch (\Exception $e){}
        });
    }
}