<?php

namespace App\Service\PageCrawler;

class PageCrawlerFactory
{
    /**
     * @param string $url
     *
     * @return PageCrawler
     */
    public static function create(string $url)
    {
        return new PageCrawler($url);
    }
}