<?php

namespace App\Service\PageCrawler;


use App\Helper\UrlHelper;
use App\Model\AnchorGroup;
use Symfony\Component\DomCrawler\Crawler;

class AnchorCrawler
{
    /** @var  Crawler $crawler */
    private $crawler;

    /** @var  string $baseUrl */
    private $baseUrl;

    /** @var  Crawler $anchors */
    private $anchors;

    /** @var int $count */
    public $count = 0;

    /** @var array $groups */
    public $groups = [];

    public function __construct(Crawler $crawler, string $baseUrl)
    {
        $this->crawler = $crawler;
        $this->baseUrl = $baseUrl;

        $this->init();
    }

    protected function init()
    {
        $this->getAllAnchors();
        $this->countAnchors();
        $this->groupAnchors();
    }

    protected function getAllAnchors()
    {
        $this->anchors = $this->crawler->filter('a');
    }

    protected function countAnchors()
    {
        if (!$this->anchors) {
            return;
        }

        $this->count = $this->anchors->count();
    }

    protected function groupAnchors()
    {
        if (!$this->anchors) {
            return;
        }

        $this->anchors->reduce(function (Crawler $node, $i) {
            if (!$href = $node->attr('href')) {
                return;
            }
            $this->setAnchorDetails($href);
        });
    }

    /**
     * @param string $href
     */
    protected function setAnchorDetails(string $href)
    {
        if ($href == "") {
            $this->setGroup('internal');
            return;
        }

        // anchor link ex: #tab1 #tab2 #section3
        //TODO some javascript frameworks have internal urls starting with # (www.example.com/#/app/dashboard)
        //TODO for these we will need some extra checks
        if ($href[0] == "#") {
            $this->setGroup('anchor');
            return;
        }

        if (UrlHelper::isContactUrl($href)){
            $this->setGroup('contact');
            return;
        }

        //ex: /blog, /, /category/1
        if ( ! filter_var($href, FILTER_VALIDATE_URL)) {
            $this->setGroup('internal');
            return;
        }

        $hostname = parse_url($href, PHP_URL_HOST);
        if (empty($hostname)) {
            $this->setGroup('internal');
            return;
        }

        if (gethostbyname($hostname) == $this->getBaseUrlHostName()) {
            $this->setGroup('internal');
            return;
        }

        $this->setGroup('external' , $hostname);

    }

    /**
     * @return string
     * @throws \Exception
     */
    protected function getBaseUrlHostName()
    {
        $hostname = parse_url($this->baseUrl, PHP_URL_HOST);
        if (empty($hostname)) {
            return false;
        }

        return gethostbyname($hostname);
    }

    /**
     * @param string      $type
     * @param string|null $domain
     */
    protected function setGroup(string $type, string $domain = null)
    {
        $group = isset($this->groups[$type]) ? $this->groups[$type] : new AnchorGroup();

        if (!$group instanceof AnchorGroup) {
            return;
        }

        $group->count++;

        if ($domain !== null && !in_array($domain, $group->domains)) {
            $group->domains[] = $domain;
        }

        $this->groups[$type] = $group;
    }

}